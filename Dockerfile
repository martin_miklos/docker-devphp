FROM nginx:latest

MAINTAINER Martin Miklós <miklos.martin@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# nginx conf tweak
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN sed -e 's/user\s*nginx/user www-data/g' -i /etc/nginx/nginx.conf

# install php
ADD dotdeb.list /etc/apt/sources.list.d/dotdeb.list
RUN apt-get update && apt-get upgrade -y && apt-get install -y --force-yes --fix-missing php7.0-cli php7.0-fpm \
        php7.0-curl php7.0-gd php7.0-geoip php7.0-imagick php7.0-imap php7.0-intl \
        php7.0-mcrypt php7.0-memcache php7.0-memcached php7.0-redis \
        php7.0-mysql php7.0-pgsql php7.0-sqlite php7.0-apcu php7.0-apcu-bc php7.0-xdebug

# and composer
RUN wget -O- https://getcomposer.org/installer | php -- --filename=composer --install-dir=/bin

# forward fpm logs to docker log collector
RUN ln -sf /dev/stdout /var/log/php7.0-fpm.log

# require and enable custom ini settings on builds
ONBUILD ADD custom.php.ini /etc/php/mods-available/custom.ini
ONBUILD RUN ln -s /etc/php/mods-available/custom.ini /etc/php/7.0/fpm/conf.d/99-custom.ini
ONBUILD RUN ln -s /etc/php/mods-available/custom.ini /etc/php/7.0/cli/conf.d/99-custom.ini

