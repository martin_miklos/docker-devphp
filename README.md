# Dev env base

Base image for eased php development.
Includes nginx, and php5.
Built from the official [nginx](https://registry.hub.docker.com/u/library/nginx/) image.

## Building from this image

There must be a `custom.php.ini` file to successfully build from this image.
There you can and should set for example the `date.timezone` and any `php.ini` directives you use for your own dev environment.

